package dataProvider;

import enums.DriverType;
import enums.EnvironmentType;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Classe pour gerer le webdriver
 */
public class WebDriverManager {
    private WebDriver driver;
    private DriverType driverType;
    private EnvironmentType environmentType;
    public Capabilities capabilities;

    public WebDriverManager(){
        driverType = FileReaderManager.getConfigFileReader().getBrowser();
        environmentType = FileReaderManager.getConfigFileReader().getEnvironment();
    }

    /**
     * methode renvoyant le driver
     * @return WebDriver
     */
    public WebDriver getDriver(){
        if(driver == null) {
            driver = createDriver();
        }
        return driver;
    }

    /**
     *  methode generant le driver local ou de prod
     * @return WebDriver
     */
    private WebDriver createDriver(){
        switch (environmentType){
            case LOCAL: driver = createLocalDriver();
            break;
            case PRODUCTION: driver = createProdDriver();
            break;
        }
        return driver;
    }

    /**
     * methode renvoyant le driver de production
     * @return WebDriver
     */
    private WebDriver createProdDriver() {
        switch (driverType){
            case CHROME: driver = new ChromeDriver();
                break;
            case FIREFOX:driver = new FirefoxDriver();
                break;
            case INTERNET_EXPLORER:driver = new InternetExplorerDriver();
                break;
        }
        if(FileReaderManager.getConfigFileReader().isBrowserWindowSize()){
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(FileReaderManager.getConfigFileReader().getImplicitlyWait(), TimeUnit.SECONDS);
        return driver;
    }

    /**
     * methode renvoyant le driver local
     * @return WebDriver
     */
    private WebDriver createLocalDriver(){
        switch (driverType){
            case CHROME: driver = new ChromeDriver();
                break;
            case FIREFOX:driver = new FirefoxDriver();
                break;
            case INTERNET_EXPLORER:driver = new InternetExplorerDriver();
                break;
        }
        if(FileReaderManager.getConfigFileReader().isBrowserWindowSize()){
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(FileReaderManager.getConfigFileReader().getImplicitlyWait(), TimeUnit.SECONDS);
        return driver;
    }

    /**
     * methode permettant d'arreter le driver
     */
    public void closeDriver(){
        driver.close();
    }
}

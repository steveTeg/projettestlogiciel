package dataProvider;

public class FileReaderManager {
    private static FileReaderManager fileReaderManager = new FileReaderManager();
    private static ConfigFileReader configFileReader;

    public FileReaderManager(){
    }

    public static FileReaderManager getInstance(){
        return fileReaderManager;
    }

    public static ConfigFileReader getConfigFileReader(){
        return configFileReader == null ? new ConfigFileReader()  : configFileReader;
    }
}

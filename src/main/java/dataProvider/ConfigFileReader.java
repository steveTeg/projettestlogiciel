package dataProvider;

import enums.DriverType;
import enums.EnvironmentType;

import java.io.*;
import java.util.Properties;

/**
 * Classe permettant de lire la valeur des clés du fichier configuration.properties
 */
public class ConfigFileReader {

    private Properties properties;
    private static final String PROPERTY_FILE_PATH = "config//configuration.properties";

    public  ConfigFileReader(){
        BufferedReader reader;
        try{
            reader = new BufferedReader(new FileReader(PROPERTY_FILE_PATH));
            properties = new Properties();
            try{
                properties.load(reader);
                reader.close();
            }catch (IOException e){
                throw new RuntimeException("erreur lors du chargement du fichier");
            }
        } catch(FileNotFoundException e){
            throw  new RuntimeException("la configuration.properties n'existe pas pour le chemin "+ PROPERTY_FILE_PATH);
        }
    }


    /**
     * methode renvoyant le temps d'attente du webdriver avant de lancer l'exception
     * @return long
     */
    public long getImplicitlyWait(){
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if(implicitlyWait != null && !"".equals(implicitlyWait)){
            return Long.parseLong(implicitlyWait);
        }else{
            throw  new RuntimeException("L'attente implicite n'est pas spécifié");
        }
    }

    /**
     * methode renvoyant l'url du site testé
     * @return String
     */
    public String getApplicationUrl(){
        String url = properties.getProperty("url");
        if(url != null && !"".equals(url)){
            return url;
        }else{
            throw new RuntimeException("L'url n'est pas spécifié");
        }
    }

    /**
     * methode renvoyant le type de navigateur à utiliser
     * @return DriverType
     */
    public DriverType getBrowser(){
        String browserName = properties.getProperty("browser");
        DriverType result;
        switch (browserName) {
            case "chrome":
                result = DriverType.CHROME;
                break;
            case "firefox":
                result = DriverType.FIREFOX;
                break;
            case "iexplorer":
                result = DriverType.INTERNET_EXPLORER;
                break;
            default:
                result = DriverType.CHROME;
                break;
        }
        return result;
    }

    /**
     *  methode renvoyant l'environnement de test
     * @return EnvironmentType
     */
    public EnvironmentType getEnvironment(){
        String environmentName = properties.getProperty("environment");
        if(environmentName == null || environmentName.equalsIgnoreCase("local")){
            return EnvironmentType.LOCAL;
        }else if(environmentName.equals("production")){
            return EnvironmentType.PRODUCTION;
        }else{
            throw  new RuntimeException("L'environement spécifié ne correspond à aucun environement");
        }
    }

    /**
     *  methode renvoyant un boolean pour savoir si le navigateur doit etre agrandi ou pas
     * @return Boolean
     */
    public Boolean isBrowserWindowSize(){
        String windowSize = properties.getProperty("windowMaximize");
        if(windowSize != null){
            return Boolean.valueOf(windowSize);
        }
        return true;
    }

    public void changeValueProperties(String key, String value){
        try(FileInputStream in = new FileInputStream(PROPERTY_FILE_PATH)) {
            Properties props = new Properties();
            props.load(in);
            in.close();

            FileOutputStream out = new FileOutputStream(PROPERTY_FILE_PATH);
            props.setProperty(key, value);
            props.store(out, null);
            out.close();
        }catch(IOException ex){
            throw  new RuntimeException("Erreur lors de la  modification de valeur du fichier " + PROPERTY_FILE_PATH);
        }
    }
}

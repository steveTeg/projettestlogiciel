package manager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import pageObjects.*;

public class PageObjectManager {
    private WebDriver driver;
    private Actions actions;
    private Homepage homepage;
    private SearchPage searchPage;
    private ListArticlesPage listArticlesPage;
    private DetailArticlePage detailArticlePage;
    private CritereRecherchePage critereRecherchePage;

    public PageObjectManager(WebDriver driver , Actions actions){
        this.driver = driver;
        this.actions = actions;
    }

    public Homepage getHomepage(){
        return homepage == null ? new Homepage(driver) : homepage;
    }

    public  SearchPage getSearchPage(){
        return searchPage == null ? new SearchPage(driver) : searchPage;
    }

    public ListArticlesPage getListArticlesPage(){
        return listArticlesPage == null ? new ListArticlesPage(driver) : listArticlesPage;
    }

    public DetailArticlePage getDetailArticlePage(){
        return detailArticlePage == null ? new DetailArticlePage(driver) : detailArticlePage;
    }

    public CritereRecherchePage getCritereRecherchePage(){
        return critereRecherchePage == null ? new CritereRecherchePage(driver, actions) : critereRecherchePage;
    }
}

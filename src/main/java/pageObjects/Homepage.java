package pageObjects;

import dataProvider.FileReaderManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Homepage {
    private WebDriver driver;

    public Homepage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void getHomePage(){
        driver.get(FileReaderManager.getConfigFileReader().getApplicationUrl());
    }
}

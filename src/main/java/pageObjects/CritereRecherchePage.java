package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CritereRecherchePage {
    private WebDriver driver;
    private Actions actions;
    @FindBy(xpath = "//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[1]/div/button")
    private WebElement btnTrier;
    @FindBy(className ="_2EAcS_V")
    private WebElement pageTomove;
    @FindBy(xpath ="//*[@id=\"plp_web_sort_price_high_to_low\"]")
    private WebElement elementSelect;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[2]/div/button")
    private WebElement btnPourcentageReduction;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[2]/div/div")
    private WebElement pagePourcentageReduction;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[2]/div/div/div/ul/li[1]")
    private WebElement selectPourcentageReduction;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[4]/div/button")
    private WebElement btnCategorie;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[4]/div/div")
    private WebElement pageCategorie;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[4]/div/div/div/ul/li[1]")
    private WebElement selectCategorie;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[6]/div/button")
    private WebElement btnStyle;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[6]/div/div")
    private WebElement pageStyle;
    @FindBy(xpath ="//*[@id=\"plp\"]/div/div[2]/div/div/div/div[1]/ul/li[6]/div/div/div/ul/li[2]")
    @CacheLookup
    private WebElement selectStyle;

    public CritereRecherchePage(WebDriver driver, Actions actions){
        this.driver = driver;
        this.actions = actions;
        PageFactory.initElements(driver, this);
    }

    public void clickButtonTrier(){
        btnTrier.click();
    }

    public void moveToListselection(){
        actions.moveToElement(pageTomove).perform();
    }

    public void selectElementTrier(){
        elementSelect.click();
    }

    public void clickButtonPourcentageReduction(){
        btnPourcentageReduction.click();
    }

    public void moveToPagePourcentageReduction(){
        actions.moveToElement(pagePourcentageReduction).perform();
    }

    public void selectPourcentageReduction(){
        selectPourcentageReduction.click();
    }

    public void clickButtonCategorie(){
        btnCategorie.click();
    }

    public void moveToPageCategorie(){
        actions.moveToElement(pageCategorie).perform();
    }

    public void selectCategorie(){
        selectCategorie.click();
    }

    public void clickButtonStyle(){
        btnStyle.click();
    }

    public void moveToPageStyle(){
        actions.moveToElement(pageStyle).perform();
    }

    public void selectStyle(){
        selectStyle.click();
    }
}

package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DetailArticlePage {
    private WebDriver driver;
    @FindBy(css = "select#main-size-select-0")
    @CacheLookup
    private WebElement selectSizeArticle;
    @FindBy(xpath = "//*[@id=\"product-add-button\"]")
    private WebElement ajoutBtnElement;

    public DetailArticlePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void selectSecondSize(){
        selectSizeArticle.click();
        Select select = new Select(selectSizeArticle);
        select.selectByIndex(2);
    }

    public void ajoutArticle(){
        ajoutBtnElement.click();
    }
}

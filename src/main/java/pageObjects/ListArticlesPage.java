package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ListArticlesPage {

    private WebDriver driver;

    @FindBy(className="_3YREj-P")
    private WebElement sections;

    public ListArticlesPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void selectFirstArticle() throws Exception {
        getArticle().click();
    }

    public String getNomArticle() throws Exception {
        return getArticle().findElement(By.tagName("h2")).getText();
    }

    private WebElement getArticle() throws Exception {
        List<WebElement> articles = sections.findElements(By.tagName("article"));
        if(articles != null && articles.size() > 0) {
           return articles.get(0);
        }else{
            throw new Exception("il n'ya aucun article disponible");
        }
    }


}

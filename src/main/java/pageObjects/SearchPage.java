package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {

    private WebDriver driver;

    @FindBy(name="q")
    @CacheLookup
    private WebElement searchElement;

    public SearchPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void searchArticle(String item){
        searchElement.sendKeys(item);
    }

    public void clickButton(){
        searchElement.sendKeys(Keys.ENTER);
    }
}

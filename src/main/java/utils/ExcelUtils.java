package utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * methode permettant de traiter notre fichier excel
 */
public class ExcelUtils {
    private static final String EXCEL_FILE_FILEPATH= "testData//data.xlsx";
    private static XSSFWorkbook workbook;
    private static XSSFSheet sheet;
    private static XSSFRow row;
    private static XSSFCell cell;

    /** methode permettant de charger le fichier excel **/
    public static void setExcelFile(String sheetName) throws IOException{
        File file = new File(EXCEL_FILE_FILEPATH);
        FileInputStream inputStream = new FileInputStream(file);
        workbook = new XSSFWorkbook(inputStream);
        sheet = workbook.getSheet(sheetName);
    }

    /**
     * Renvoie la valeur de la cellule selectionnée
     * @param rowNumber
     * @param cellNumber
     * @return
     */
    public static String getCellData(int rowNumber, int  cellNumber){
        cell = sheet.getRow(rowNumber).getCell(cellNumber);
        return cell.getStringCellValue();
    }

    /**
     * Renvoie le nombre de ligne du fichier excel
     * @return int
     */
    public static int getRowCountInSheet(){
        int rowcount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        return rowcount;
    }

}

package projetTestLogiciel;

import dataProvider.WebDriverManager;
import manager.PageObjectManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import pageObjects.*;

import java.util.List;

/**
 * Classe principale
 *
 */
public class App 
{
    public static void main( String[] args )
            throws Exception {

        WebDriverManager webDriverManager = new WebDriverManager();
        WebDriver driver = webDriverManager.getDriver();
        Actions actions  = new Actions(driver);

        PageObjectManager pageObjectManager = new PageObjectManager(driver, actions);
        Homepage homepage = pageObjectManager.getHomepage();
        homepage.getHomePage();

        SearchPage searchPage = pageObjectManager.getSearchPage();
        searchPage.searchArticle("Veste Homme");
        searchPage.clickButton();

        CritereRecherchePage critereRecherchePage = pageObjectManager.getCritereRecherchePage();
        critereRecherchePage.clickButtonTrier();
        critereRecherchePage.moveToListselection();
        critereRecherchePage.selectElementTrier();

        critereRecherchePage.clickButtonPourcentageReduction();
        critereRecherchePage.moveToPagePourcentageReduction();
        critereRecherchePage.selectPourcentageReduction();

        critereRecherchePage.clickButtonCategorie();
        critereRecherchePage.moveToPageCategorie();
        critereRecherchePage.selectCategorie();

        critereRecherchePage.clickButtonStyle();
        critereRecherchePage.moveToPageStyle();
        critereRecherchePage.selectStyle();
        critereRecherchePage.clickButtonStyle();

        ListArticlesPage listArticlesPage = pageObjectManager.getListArticlesPage();
        listArticlesPage.selectFirstArticle();

        DetailArticlePage detailArticlePage = pageObjectManager.getDetailArticlePage();
        detailArticlePage.selectSecondSize();
        detailArticlePage.ajoutArticle();
        webDriverManager.closeDriver();
     }
}

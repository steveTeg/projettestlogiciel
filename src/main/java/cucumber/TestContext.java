package cucumber;

import dataProvider.WebDriverManager;
import manager.PageObjectManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class TestContext {
    private WebDriverManager webDriverManager;
    private PageObjectManager pageObjectManager;
    public ScenarioContext scenarioContext;

    public TestContext(){
        webDriverManager = new WebDriverManager();
        WebDriver driver = webDriverManager.getDriver();
        scenarioContext = new ScenarioContext();
        Actions actions = new Actions(driver);
        pageObjectManager = new PageObjectManager(driver, actions);
    }

    public WebDriverManager getWebDriverManager() {
        return webDriverManager;
    }

    public PageObjectManager getPageObjectManager() {
        return pageObjectManager;
    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }

}

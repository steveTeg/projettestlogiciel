Feature: Order an article.

 Scenario Outline: adding an item to the shopping cart
      Given user is a Home page
      When he search for "<item>"
      And sort by price
      And sort by percentage reduction
      And sort by category
      And sort by Style
      And select the first article
      And select the second size of the article
      And verify name of item
      And add the item to the shopping cart

      Examples:
          |item|
          |Veste Homme|
package runners;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/functionalTests",
        glue= {"stepDefinitions"},
        plugin = { "com.cucumber.listener.ExtentCucumberFormatter:src/test/resources/rapports/report.html"},
        monochrome = true
)
public class TestRunner {
    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig(new File("config/extend-config.xml"));
        Reporter.setSystemInfo("Machine", 	"ubuntu.14.04.6-deskstop" + "64 Bit");
        Reporter.setSystemInfo("Selenium", "4.0.0-alpha-1");
        Reporter.setSystemInfo("Maven", "3.3.9");
        Reporter.setSystemInfo("Java Version", "11.0.12");
    }

}

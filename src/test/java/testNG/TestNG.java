package testNG;

import baseTest.BaseTest;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.Homepage;
import pageObjects.SearchPage;

public class TestNG extends BaseTest {
    private Homepage homepage;
    private SearchPage searchPage;

    @Test
    public void titleWebSite() {
        homepage = pageObjectManager.getHomepage();
        homepage.getHomePage();
        String testTitle = "ASOS | Site de Vêtements | Tendances Mode";
        String titleSite = getDriver().getTitle();
        Assert.assertEquals(titleSite, testTitle);
    }


    @Test(dependsOnMethods = { "titleWebSite" })
    @Parameters({"item"})
    public void itemSearch(@Optional("Veste Homme") String item){
        searchPage = pageObjectManager.getSearchPage();
        searchPage.searchArticle(item);
        searchPage.clickButton();
        String itemTest = getDriver().findElement(By.className("vp-JnyG")).getText();
        itemTest = StringUtils.lowerCase(itemTest);
        item = "«"+ StringUtils.lowerCase(item)+"»";
        Assert.assertEquals(itemTest,item);
    }
}

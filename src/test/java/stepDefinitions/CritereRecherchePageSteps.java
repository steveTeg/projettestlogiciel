package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.When;
import pageObjects.CritereRecherchePage;

public class CritereRecherchePageSteps {
    TestContext testContext;
    CritereRecherchePage critereRecherchePage;

    public CritereRecherchePageSteps(TestContext testContext){
        testContext = testContext;
        critereRecherchePage = testContext.getPageObjectManager().getCritereRecherchePage();
    }

    @When("^sort by price$")
    public void sort_by_price() {
        critereRecherchePage.clickButtonTrier();
        critereRecherchePage.moveToListselection();
        critereRecherchePage.selectElementTrier();
    }

    @When("^sort by percentage reduction$")
    public void sort_by_percentage_reduction()  {
        critereRecherchePage.clickButtonPourcentageReduction();
        critereRecherchePage.moveToPagePourcentageReduction();
        critereRecherchePage.selectPourcentageReduction();
    }

    @When("^sort by category$")
    public void sort_by_category() {
        critereRecherchePage.clickButtonCategorie();
        critereRecherchePage.moveToPageCategorie();
        critereRecherchePage.selectCategorie();
    }

    @When("^sort by Style$")
    public void sort_by_Style() {
        critereRecherchePage.clickButtonStyle();
        critereRecherchePage.moveToPageStyle();
        critereRecherchePage.selectStyle();
        critereRecherchePage.clickButtonStyle();
    }
}

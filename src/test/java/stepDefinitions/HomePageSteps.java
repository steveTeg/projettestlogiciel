package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import pageObjects.Homepage;

public class HomePageSteps {
    TestContext testContext;
    Homepage homePage;

    public HomePageSteps(TestContext testContext){
        testContext = testContext;
        homePage = testContext.getPageObjectManager().getHomepage();
    }

    @Given("^user is a Home page$")
    public void user_is_a_Home_page() {
        homePage.getHomePage();
    }
}

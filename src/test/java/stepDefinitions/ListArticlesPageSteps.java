package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.When;
import enums.Context;
import pageObjects.ListArticlesPage;

public class ListArticlesPageSteps {
    TestContext testContext;
    ListArticlesPage listArticlesPage;

    public ListArticlesPageSteps(TestContext testContext){
        this.testContext = testContext;
        listArticlesPage = testContext.getPageObjectManager().getListArticlesPage();
    }

    @When("^select the first article$")
    public void select_the_first_article() throws Exception {
        testContext.scenarioContext.setContext(Context.ARTICLE_NAME, listArticlesPage.getNomArticle());
        listArticlesPage.selectFirstArticle();
    }
}

package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.When;
import pageObjects.SearchPage;

public class SearchPageSteps {
    TestContext testContext;
    SearchPage searchPage;

    public SearchPageSteps(TestContext testContext){
        testContext = testContext;
        searchPage = testContext.getPageObjectManager().getSearchPage();
    }

    @When("^he search for \"([^\"]*)\"$")
    public void he_search_for(String nomArticle){
        searchPage.searchArticle(nomArticle);
        searchPage.clickButton();
    }

}

package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.When;
import enums.Context;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import pageObjects.DetailArticlePage;

public class DetailArticlePageSteps {
    TestContext testContext;
    DetailArticlePage detailArticlePage;

    public DetailArticlePageSteps(TestContext testContext){
        this.testContext = testContext;
        detailArticlePage = testContext.getPageObjectManager().getDetailArticlePage();
    }

    @When("^select the second size of the article$")
    public void select_the_second_size_of_the_article()  {
        detailArticlePage.selectSecondSize();
    }

    @When("^verify name of item$")
    public void verify_name_of_item(){
        String articleName = (String)testContext.scenarioContext.getContext(Context.ARTICLE_NAME);
        //Ici vue qu'on teste un site en ligne on veut juste confirmer que le texte existe
        Assert.assertTrue(articleName, true);
    }

    @When("^add the item to the shopping cart$")
    public void add_the_item_to_the_shopping_cart() {
        detailArticlePage.ajoutArticle();
    }

}

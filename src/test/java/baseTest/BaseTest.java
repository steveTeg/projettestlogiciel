package baseTest;

import manager.PageObjectManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    public PageObjectManager pageObjectManager;
    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();

    @BeforeClass
    @Parameters(value={"browser"})
    public void setup (@Optional("chrome") String browser) throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("browserName", browser);
        driver.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), desiredCapabilities));
        Actions actions  = new Actions(driver.get());
        this.pageObjectManager = new PageObjectManager(driver.get(), actions);
    }

    public WebDriver getDriver() {
        return driver.get();
    }

    @AfterClass
    void terminate () {
        getDriver().quit();
        driver.remove();
    }
}

pipeline{
    agent any
    environment {
      NEXUS_VERSION = "nexus3"
      // Ici on peut mettre http ou https
      NEXUS_PROTOCOL = "https"
      // Ici j'ai mis une adresse url generé avec ngrok  pour que ça soit accessible depuis internet
      NEXUS_URL = "2a62-91-169-168-137.ngrok.io"
      // Ici on met le repertoire ou on va deposer notre artefact
      NEXUS_REPOSITORY = "maven-snapshots"
      // Le libellle de connexion du nexus dans notre jenkins
      NEXUS_CREDENTIAL_ID = "nexus-auth"
     }
    stages{
       // Cette etape permet de cloner notre projet
            stage('SCM'){
                steps{
                    checkout scm
                }
            }
            //Cette etape permet de builder le projet
            stage('Build'){
               // parallel ici c'est pour dire que la compilation et la verification checkstyle se feront au meme niveau
                parallel {
                    //Ici on rajoute un autre étape du pipeline qui consiste à compiler notre projet
                    stage('Compile') {
                        agent {
                         // Ici nous allons utiliser un agent docker pour créer un conteneur maven à fin de compiler notre projet
                          docker {
                                   image 'maven:3.3-jdk-8'
                                   args '-v /root/.m2/repository:/root/.m2/repository'
                                    // Ici reuseNode est par defaut à false mais nous mettons true pour dire qu'il utilise l'agent du dessus
                                   reuseNode true
                           }
                       }
                     steps {
                             sh ' mvn clean compile'
                        }

                    }
                  //Ici nous utilisons checkstyle pour vérifier les normes de codage java
                   stage('CheckStyle') {
                       agent {
                         docker {
                          image 'maven:3.3-jdk-8'
                          args '-v /root/.m2/repository:/root/.m2/repository'
                          reuseNode true
                         }
                      }
                      steps {
                         sh ' mvn checkstyle:checkstyle'
                      }
                   }
                }
            }
            //Ici on va jouer nos tests
            stage('Unit Tests') {
            //On dit ici que les tests ne vont etre jouer que sur la branche main et feat/projet_selenium c au choix
               when {
                anyOf { branch 'main'; branch 'feat/projet_selenium' }
               }
               agent {
                docker {
                 image 'maven:3.3-jdk-8'
                 args '-v /root/.m2/repository:/root/.m2/repository'
                 reuseNode true
                }
               }
               steps {
                sh 'mvn test'
               }
            }
            //Ici on va jouer nos test
            stage('generated jar') {
            //On dit ici que les tests ne vont etre jouer que sur la branche main et feat/projet_selenium c au choix
               when {
                anyOf { branch 'main'; branch 'feat/projet_selenium' }
               }
               agent {
                docker {
                 image 'maven:3.3-jdk-8'
                 args '-v /root/.m2/repository:/root/.m2/repository'
                 reuseNode true
                }
               }
               steps {
                sh 'mvn package'
               }
                post {
                 //Ici si tout va bien on genere le jar
                   success {
                    stash(name: 'artifact', includes: 'target/*.jar')
                    stash(name: 'pom', includes: 'pom.xml')
                    archiveArtifacts 'target/*.jar'
                   }
                 }
            }
       stage('Code Quality Analysis') {
         parallel {
         // Permet de gerer des variables non utilisées
            stage('PMD') {
               agent {
                docker {
                 image 'maven:3.3-jdk-8'
                 args '-v /root/.m2/repository:/root/.m2/repository'
                 reuseNode true
                }
              }
              steps {
                 sh ' mvn pmd:pmd'
              }
            }
          // Ici Findbugs permet de verifier les methodes privées non utilisées, les null pointeur exceptions
          stage('Findbugs') {
              agent {
                docker {
                  image 'maven:3.3-jdk-8'
                  args '-v /root/.m2/repository:/root/.m2/repository'
                  reuseNode true
                }
              }
             steps {
                sh ' mvn findbugs:findbugs'
             }
          }
         // Ici vue que mon projet est un projet java je mets une etape de javaDoc
         stage('JavaDoc') {
              agent {
                docker {
                  image 'maven:3.3-jdk-8'
                  args '-v /root/.m2/repository:/root/.m2/repository'
                  reuseNode true
                }
              }
            steps {
               sh ' mvn javadoc:javadoc'
            }
         }
       }
        post {
            always {
              recordIssues aggregatingResults: true, tools: [javaDoc(), checkStyle(pattern: '**/target/checkstyle-result.xml'), findBugs(pattern: '**/target/findbugsXml.xml', useRankAsPriority: true), pmdParser(pattern: '**/target/pmd.xml')]
            }
        }
       }
        stage('Deployer le jar dans le Nexus') {
         when {
          anyOf { branch 'main'; branch 'feat/projet_selenium'}
         }
         steps {
          script {
           unstash 'pom'
           unstash 'artifact'
           pom = readMavenPom file: "pom.xml";
           // on package le pom
           filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
           echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
           // On extrait le chemin de l'artefact
           artifactPath = filesByGlob[0].path;
           // ici c'est true si l'artefact existe
           artifactExists = fileExists artifactPath;
           if (artifactExists) {
            nexusArtifactUploader(
             nexusVersion: NEXUS_VERSION,
             protocol: NEXUS_PROTOCOL,
             nexusUrl: NEXUS_URL,
             groupId: pom.groupId,
             version: pom.version,
             repository: NEXUS_REPOSITORY,
             credentialsId: NEXUS_CREDENTIAL_ID,
             artifacts: [
              [artifactId: pom.artifactId,
               classifier: '',
               file: artifactPath,
               type: pom.packaging
              ],
              [artifactId: pom.artifactId,
               classifier: '',
               file: "pom.xml",
               type: "pom"
              ]
             ]
            )
           } else {
            error "*** File: ${artifactPath}, artefact not found";
           }
          }
         }
        }
    }
}